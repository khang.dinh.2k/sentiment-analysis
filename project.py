from selenium import webdriver
from time import sleep
from selenium.webdriver.common.keys import Keys

# 1. Setup driver Chrome
browser  = webdriver.Chrome(r"D:\HOCTAP\NAM4\DOAN\chromedriver.exe")


# 2. Open shopee

# 2b. Goto specific web
browser.get(
    "https://shopee.vn/%C3%81o-kho%C3%A1c-gi%C3%B3-cao-c%E1%BA%A5p-2-l%E1%BB%9Bp-ch%E1%BA%A5t-gi%C3%B3-tr%C3%A1ng-b%E1%BA%A1c-ch%E1%BB%91ng-n%C6%B0%E1%BB%9Bc-ch%E1%BB%91ng-gi%C3%B3-ng%C4%83n-tia-UV-tuy%E1%BB%87t-%C4%91%E1%BB%91i-i.113476725.3796451189?sp_atk=e488efde-ad24-426e-aacf-644e20e5998e")
sleep(8)

# 3. Get positive comments
Five_star_Rate = browser.find_element_by_xpath("/html/body/div[1]/div/div[2]/div[2]/div/div[3]/div[2]/div[1]/div[2]/div/div[2]/div[2]/div[2]")
Five_star_Rate.click()
sleep(1)

def get_Comment():
    list_comment = browser.find_elements_by_class_name('shopee-product-rating__main')
       # Lặp trong tất cả các comment và hiển thị nội dung comment ra màn hình
    for comment in list_comment:
        # hiển thị tên người và nội dung, cách nhau bởi dấu :
        poster = comment.find_element_by_class_name("shopee-product-rating__author-name")
        content = comment.find_element_by_class_name("shopee-product-rating__content")
        print("*", poster.text,":", content.text)
        with open('DATA.txt', "a") as data:
            data.write("{}\n".format(content.text))
            data.close()
    sleep(1)

def get_more_Comments_1():
    show_more_link = browser.find_element_by_xpath("/html/body/div[1]/div/div[2]/div[2]/div/div[3]/div[2]/div[1]/div[2]/div/div[3]/div[2]/button[8]")
    show_more_link.click()
    sleep(1)

def get_more_Comments_2():
    show_more_link = browser.find_element_by_xpath("/html/body/div[1]/div/div[2]/div[2]/div/div[3]/div[2]/div[1]/div[2]/div/div[3]/div[2]/button[9]")
    show_more_link.click()
    sleep(1)

def get_more_Comments_3():
    show_more_link = browser.find_element_by_xpath("/html/body/div[1]/div/div[2]/div[2]/div/div[3]/div[2]/div[1]/div[2]/div/div[3]/div[2]/button[10]")
    show_more_link.click()
    sleep(1)

def get_more_Comments_4():
    show_more_link = browser.find_element_by_xpath("/html/body/div[1]/div/div[2]/div[2]/div/div[3]/div[2]/div[1]/div[2]/div/div[3]/div[2]/button[11]")
    show_more_link.click()
    sleep(1)

for i in range(12):
    if i<3:
        get_Comment()
        get_more_Comments_1()
    elif i>=3 and i<5:
        get_more_Comments_2()
        get_Comment()
    elif i>=5 and i<7:
        get_more_Comments_3()
        get_Comment()
    elif i>=7:
        get_more_Comments_4()
        get_Comment()


# 4. Đóng trình duyệt
browser.close()
